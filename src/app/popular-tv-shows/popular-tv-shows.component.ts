import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

import { TheMovieDbService } from '../the-movie-db.service';
import { ItvShow } from '../itv-show';

@Component({
  selector: 'app-popular-tv-shows',
  templateUrl: './popular-tv-shows.component.html',
  styleUrls: ['./popular-tv-shows.component.css'],
  providers: [TheMovieDbService]
})
export class PopularTvShowsComponent implements OnInit {

  private tvShows: ItvShow[];

  constructor(service: TheMovieDbService) {
    service.getPopularTvShows().subscribe(results => this.tvShows = results);
  }

  ngOnInit() {
  }

}
