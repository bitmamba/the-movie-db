import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Location } from '@angular/common';

import { TheMovieDbService } from '../the-movie-db.service';
import { Imovie } from '../imovie';
import { ImovieDetail } from '../imovie-detail';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {
  private movieDetail: ImovieDetail;

  constructor(
    private service: TheMovieDbService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
    this.route.params.subscribe(params => {
      this.service
        .getMovieDetail(params['id'])
        .subscribe(result => (this.movieDetail = result));
    });
  }

  ngOnInit() {}
  // TODO switch der sikre at man går tilbage til den rigtige side.
  public gotoPrevious() {
    // this.router.navigate(['/movies-popular']);
    this.location.back();
  }
}
