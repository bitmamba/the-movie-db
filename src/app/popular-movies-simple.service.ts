import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IpopularMovie } from './ipopular-movie';
import 'rxjs/add/operator/map';

@Injectable()
export class PopularMoviesSimpleService {

  private baseUrl = 'https://api.themoviedb.org/3/movie/popular';
  private apiKey = '?api_key=609d9a54e7a2c29a11fdb2de1919a8ad';
  data: any = {};

  constructor(private http: Http) {
    /* this.getPopularMovies();
    this.getData(); */
  }

  // tslint:disable-next-line:max-line-length
 /*  public getPopularMovies(): Observable<Response> { return this.http.get('https://api.themoviedb.org/3/movie/popular?api_key=609d9a54e7a2c29a11fdb2de1919a8ad'); } */

  getData() {
    return this.http.get(this.baseUrl + this.apiKey).map((res: Response) => res.json().results);
  }

  getPopularMovies() {
    this.getData().subscribe(data => {
      console.log('RESPONSE TIL CONSOL', data);
      this.data = data;
    });
  }
}
