import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

// Components
import { AppComponent } from './app.component';
import { PopularMoviesComponent } from './popular-movies/popular-movies.component';
import { TopRatedMoviesComponent } from './top-rated-movies/top-rated-movies.component';
import { UpcomingMoviesComponent } from './upcoming-movies/upcoming-movies.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { PopularTvShowsComponent } from './popular-tv-shows/popular-tv-shows.component';
import { TvShowDetailComponent } from './tv-show-detail/tv-show-detail.component';

// Services
import { TheMovieDbService } from './the-movie-db.service';

// Modules
import { AppRouterModule } from './app-router/app-router.module';

@NgModule({
  declarations: [
    AppComponent,
    PopularMoviesComponent,
    TopRatedMoviesComponent,
    UpcomingMoviesComponent,
    MovieDetailComponent,
    PopularTvShowsComponent,
    TvShowDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    AppRouterModule
  ],
  providers: [TheMovieDbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
