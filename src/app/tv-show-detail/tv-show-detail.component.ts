import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Location } from '@angular/common';

import { TheMovieDbService } from '../the-movie-db.service';
import { ItvShow } from './../itv-show';
import { ItvShowDetail } from '../itv-show-detail';

@Component({
  selector: 'app-tv-show-detail',
  templateUrl: './tv-show-detail.component.html',
  styleUrls: ['./tv-show-detail.component.css']
})
export class TvShowDetailComponent implements OnInit {
  private tvShowDetail: ItvShowDetail;

  constructor(
    private service: TheMovieDbService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {
    this.route.params.subscribe(params => {
      this.service
        .getTvShowDetail(params['id'])
        .subscribe(result => (this.tvShowDetail = result));
    });
  }

  ngOnInit() {}
  public gotoPrevious() {
    this.location.back();
  }
}
