import { TvShowDetailComponent } from './../tv-show-detail/tv-show-detail.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { PopularMoviesComponent } from '../popular-movies/popular-movies.component';
import { TopRatedMoviesComponent } from '../top-rated-movies/top-rated-movies.component';
import { UpcomingMoviesComponent } from '../upcoming-movies/upcoming-movies.component';
import { PopularTvShowsComponent } from '../popular-tv-shows/popular-tv-shows.component';
import { MovieDetailComponent } from '../movie-detail/movie-detail.component';

const appRoutes: Routes = [
  { path: 'movies-popular', component: PopularMoviesComponent },
  { path: 'movies-toprated', component: TopRatedMoviesComponent},
  { path: 'movies-upcoming', component: UpcomingMoviesComponent},
  { path: 'movie-detail/:id', component: MovieDetailComponent },
  { path: 'tv-popular', component: PopularTvShowsComponent},
  { path: 'tv-show-detail/:id', component: TvShowDetailComponent },
  { path: '', redirectTo: '/movies-popular', pathMatch: 'full'},
  { path: '**', component: PopularMoviesComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [],
  exports: [RouterModule],
})
export class AppRouterModule { }
