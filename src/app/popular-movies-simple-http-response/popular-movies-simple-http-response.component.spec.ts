import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopularMoviesSimpleHttpResponseComponent } from './popular-movies-simple-http-response.component';

describe('PopularMoviesSimpleHttpResponseComponent', () => {
  let component: PopularMoviesSimpleHttpResponseComponent;
  let fixture: ComponentFixture<PopularMoviesSimpleHttpResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopularMoviesSimpleHttpResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopularMoviesSimpleHttpResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
