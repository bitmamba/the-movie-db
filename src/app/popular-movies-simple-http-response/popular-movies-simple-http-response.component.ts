import { Component, OnInit } from '@angular/core';
import { IpopularMovie } from '../ipopular-movie';
import { PopularMoviesSimpleService } from '../popular-movies-simple.service';

@Component({
  selector: 'app-popular-movies-simple-http-response',
  templateUrl: './popular-movies-simple-http-response.component.html',
  styleUrls: ['./popular-movies-simple-http-response.component.css'],
  providers: [PopularMoviesSimpleService]
})
export class PopularMoviesSimpleHttpResponseComponent implements OnInit {

  private popularMovies: IpopularMovie[];

  constructor(service: PopularMoviesSimpleService) {
    // service.getPopularMovies().subscribe(response => this.popularMovies = response.json().results);
    service.getPopularMovies();
    service.getData();
  }

  ngOnInit() {
  }

}
