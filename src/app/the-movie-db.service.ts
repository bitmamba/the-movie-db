import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import { Imovie } from './imovie';
import { ImovieDetail } from './imovie-detail';
import { ItvShow } from './itv-show';
import { ItvShowDetail } from './itv-show-detail';

@Injectable()
export class TheMovieDbService {
  // private request$: Observable<Imovie[]>;
  private apikey = '?api_key=609d9a54e7a2c29a11fdb2de1919a8ad';
  private baseUrl = 'https://api.themoviedb.org/3/';
  private movie = 'movie/';
  private popular = 'popular';
  private topRated = 'top_rated';
  private upcoming = 'upcoming';
  private tv = 'tv/';
  // private movie_id: number;

  constructor(private http: HttpClient) {}

  public getPopularMovies() {
    return this.http
      .get(this.baseUrl + this.movie + this.popular + this.apikey)
      .map(response => <Imovie[]>response['results']);
  }

  public getTopRatedMovies() {
    return this.http
      .get(this.baseUrl + this.movie + this.topRated + this.apikey)
      .map(response => <Imovie[]>response['results']);
  }

  public getUpcomingMovies() {
    return this.http
      .get(this.baseUrl + this.movie + this.upcoming + this.apikey)
      .map(response => <Imovie[]>response['results']);
  }

  public getMovieDetail(id: number) {
    return this.http
    .get('https://api.themoviedb.org/3/movie/' + id + '?api_key=609d9a54e7a2c29a11fdb2de1919a8ad')
    .map(response => <ImovieDetail>response);
    // .get(this.baseUrl + this.movie + id + this.apikey)
  }

  public getPopularTvShows() {
    return this.http
      .get(this.baseUrl + this.tv + this.popular + this.apikey)
      .map(response => <ItvShow[]>response['results']);
  }

  public getTvShowDetail(id: number) {
    return this.http
    .get('https://api.themoviedb.org/3/tv/' + id + '?api_key=609d9a54e7a2c29a11fdb2de1919a8ad')
    .map(response => <ItvShowDetail>response);
    // .get(this.baseUrl + this.movie + id + this.apikey)
  }
}

/* constructor(http: Http) {
  // tslint:disable-next-line:max-line-length
  this.request$ = http
    .get('https://api.themoviedb.org/3/movie/popular?api_key=609d9a54e7a2c29a11fdb2de1919a8ad')
    .map(response => response.json().results);
}

public getPopularMovies(): Observable<Imovie[]> {
  return this.request$;
} */
