import { TestBed, inject } from '@angular/core/testing';

import { PopularMoviesSimpleService } from './popular-movies-simple.service';

describe('PopularMoviesSimpleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PopularMoviesSimpleService]
    });
  });

  it('should be created', inject([PopularMoviesSimpleService], (service: PopularMoviesSimpleService) => {
    expect(service).toBeTruthy();
  }));
});
