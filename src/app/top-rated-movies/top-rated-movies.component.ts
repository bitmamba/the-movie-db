import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

import { TheMovieDbService } from '../the-movie-db.service';
import { Imovie } from '../imovie';

@Component({
  selector: 'app-top-rated-movies',
  templateUrl: './top-rated-movies.component.html',
  styleUrls: ['./top-rated-movies.component.css'],
  providers: [TheMovieDbService]
})
export class TopRatedMoviesComponent implements OnInit {
  private movies: Imovie[];

  constructor(
    service: TheMovieDbService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    service.getTopRatedMovies().subscribe(results => (this.movies = results));
  }

  ngOnInit() {}
}
