import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

import { TheMovieDbService } from '../the-movie-db.service';
import { Imovie } from '../imovie';

@Component({
  selector: 'app-upcoming-movies',
  templateUrl: './upcoming-movies.component.html',
  styleUrls: ['./upcoming-movies.component.css'],
  providers: [TheMovieDbService]
})
export class UpcomingMoviesComponent implements OnInit {
  private movies: Imovie[];

  constructor(
    service: TheMovieDbService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    service.getUpcomingMovies().subscribe(results => (this.movies = results));
  }

  ngOnInit() {}
}
