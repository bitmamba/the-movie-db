import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { DatePipe } from '@angular/common'; // makes it possible to format a date.

import { TheMovieDbService } from '../the-movie-db.service';
import { Imovie } from '../imovie';

@Component({
  selector: 'app-popular-movies',
  templateUrl: './popular-movies.component.html',
  styleUrls: ['./popular-movies.component.css'],
  providers: [TheMovieDbService]
})
export class PopularMoviesComponent implements OnInit {
  private movies: Imovie[];

  constructor(
    service: TheMovieDbService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    service.getPopularMovies().subscribe(results => (this.movies = results));
  }

  ngOnInit() {}

  public onSelect(movie: Imovie) {
    this.router.navigate(['/movie-detail/', movie.id]);
  }
}
